# README #



### What is this repository for? ###

* This python script implements the known compartmental model Susceptible Exposed Infected Recovered (SEIR) for dynamic networks.
* 1.0

### How do I get set up? ###

* This script was implemented in Python version 3.8
* Dependencies: numpy, arparse and dynetx

		pip install numpy

		pip install argparse
		
		pip install dynetx

* How to run
	
	python3 script_SEIR.py
	
	arguments:

		1. --input Temporal/Dynamic network file: must contain three columns: 1st and 2nd column are the node IDs and the 3rd column is the time slot (all columns must be integer). The script assumes that the network correspond to a period of 1 day.
		2. --r_zero The R_0 value of the simulation
		3. --s_duration The time slot duration of the input network in minutes
		4. --d_recovery The number of days that elapses between the day the node becomes infected and the day it recovers.
		5. --d_incubation The number of days that elapses between the day the nodes was exposed to the disease and the day it becomes infected (develops symptoms).
		6. --num_infected The number of nodes that are already infected before the begining of the simulation
		11. -h (--help) Displays the help information of the script
		
	Example: python3 script_SEIR.py --i network.txt --r_zero 2.58 --s_duration 5 --d_recovery 14 --d_incubation 3 --num_infected 10
	
	