# -*- coding: utf-8 -*-
"""
Created on Sun Oct  9 14:44:10 2016

@author: Marco
"""

import numpy as np
import argparse
import dynetx as dn
import os

def run_SEIR(dg, i_status, beta, gamma, epsilon):
    iterations = []
    o_status = i_status
    slots = dg.temporal_snapshots_ids()
    nodes = dg.nodes()
    
    for slot in slots:
        iter_dict = dict()
        slot_status = dict()
        for n in nodes:
            slot_status[n] = o_status[n]
            neighbors = dg.neighbors(n, t=slot)
            
            if o_status[n] == 2: #Infected = 2
                for neighbor in neighbors:
                    if o_status[neighbor] == 0: #Susceptible = 0
                        eventp = np.random.random_sample()
                        if eventp < beta:
                            o_status[neighbor] = 1#Exposed
                            
                eventp = np.random.random_sample()
                if eventp < gamma:
                    o_status[n] = 3 #Removed = 3
            elif o_status[n] == 1: #Exposed
                eventp = np.random.random_sample()
                if eventp < epsilon:
                    o_status[n] = 2 #Infected
        
        iter_dict['status'] = o_status.copy()
        iterations.append(iter_dict)
        
    return iterations

#process input file
def get_data(input_file):
    
    dnet = dn.read_snapshots(input_file, nodetype=int, timestamptype=int)
    slots = dnet.temporal_snapshots_ids()
    nodes = dnet.nodes()
 
    return dnet, nodes, slots
            
#file naming
parser = argparse.ArgumentParser(description="Properties Plotter for Human Contact Networks", epilog="Authors: Marco Antonio Rodriguez Flores")
parser.add_argument('--input', help='Input Folder', required=True)
parser.add_argument('--r_zero', help='R0 value', required=True, type=float)
parser.add_argument('--s_duration', help='snapshot duration in minutes', required=True, type=float)
parser.add_argument('--d_recovery', help='number of days to recover', required=True, type=float)
parser.add_argument('--d_incubation', help='number of days of incubations', required=True, type=float)
parser.add_argument('--num_infected', help='Number of initial infected nodes', required=True, type=int)
args =parser.parse_args()

input_file = args.input
recovery_days = args.d_recovery #Days to recover
incubation_days = args.d_incubation #Days to become infected after being exposed
snap_duration = args.s_duration #minutes

output_seir = os.path.join("results")

if not os.path.exists(output_seir):
    os.makedirs(output_seir)
    
network, nodes, slots = get_data(input_file)

epsilon = float(snap_duration)/float(incubation_days*24*60)
gamma = float(snap_duration)/float(recovery_days*24*60)


N = len(nodes)

R0 = float(args.r_zero)
beta = R0*gamma

print("R0: "+str(R0))
print("beta: "+str(beta))
print("epsilon: "+str(epsilon))
print("gamma: "+str(gamma))

cumul_infected = dict()
daily_infected = dict()
active_cases = dict()

#Initially infected nodes
num_infected = args.num_infected
if num_infected > N:
    print("Error! Number of initial infected nodes is larger than the total number of nodes in the input network!")
    exit()
    
infected_nodes = np.random.choice(nodes, num_infected, replace=False)

#Initialize status of all nodes in all days
final_status = dict() #Stores the final status of the node (SEIR)
for n in nodes:
    final_status[n] = 0 #Susceptible
    
for n in infected_nodes:
    final_status[n] = 2 #Infected in SEIR model

print("Please wait, running SEIR simulation...")
iterations = run_SEIR(network, final_status, beta, gamma, epsilon)
        
f_res = open(output_seir+'/SEIR_results.txt', "w")
f_res.write("ID\tStatus(0=Susceptible, 1=Exposed, 2=Infected, 3=Recovered)\n")
Sus_t = 0 #Susceptible at day t
Exp_t = 0 #Exposed at day t
Inf_t = 0 #Infected at day t
Rec_t = 0
for n in final_status:
    f_res.write(str(n)+"\t"+str(final_status[n])+"\n")
    if final_status[n] == 0: #Susceptible
        Sus_t += 1
    elif final_status[n] == 1: #Exposed
        Exp_t += 1
    elif final_status[n] == 2: #Infected
        Inf_t += 1
    elif final_status[n] == 3: #Recovered
        Rec_t += 1
        
f_res.write("Susceptible="+str(Sus_t)+" nodes, Exposed="+str(Exp_t)+" nodes, Infected="+str(Inf_t)+" nodes, Recovered="+str(Rec_t)+" nodes.\n")
f_res.write("R_0="+str(R0))
f_res.close()
print("Completed!\a")